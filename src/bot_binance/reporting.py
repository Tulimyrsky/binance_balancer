import uvicorn
from fastapi import FastAPI
from fastapi.responses import HTMLResponse

current_report = ""

web_app = FastAPI()


def run_web_app() -> None:
    uvicorn.run(
        web_app,
        host="0.0.0.0",
        port=8002,
    )


@web_app.get("/")
def read_root() -> HTMLResponse:
    return HTMLResponse(current_report)


def log_trade_order(json_to_log):
    global current_report
    current_log = (
        f'{json_to_log["symbol"]} / {json_to_log["side"]} '
        f'/ {json_to_log["price"]} \n'
    )
    current_report += current_log
    with open("history.html", "w+") as log_file:
        log_file.write(current_log)
