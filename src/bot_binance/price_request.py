import logging
import time
from decimal import Decimal
from pprint import pprint
from threading import Thread
from typing import Dict, Optional

from bot_binance import reporting, wrapper
from bot_binance.asset import Asset
from bot_binance.wrapper import BinanceException

threshold = Decimal(0.5)

wanted_stablecoin = "BUSD"

wanted_asset_in_bot = {
    "BTC": Decimal(0),
    # "LTC": Decimal(0),
    # "LINK": Decimal(0),
    # "DOGE": Decimal(0),
    # "XVG": Decimal(0),
    # "XLM": Decimal(0),
    # "EOS": Decimal(0),
    "BNB": Decimal(40),
    # "MCO": Decimal(0),
}

wanted_usd_in_bot = Decimal(10)
total_portfolio_value = Decimal(0)

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.INFO,
)


SIDE_SELL = "SELL"
SIDE_BUY = "BUY"


def main():
    Thread(target=reporting.run_web_app, args=(), daemon=True).start()
    prices = wrapper.get_all_tickers()
    prices_dict = {prices["symbol"]: prices["price"] for prices in prices}
    portfolio = get_portfolio(prices_dict)
    all_assets_percentage_in_btc(portfolio)
    get_market_rules(portfolio)
    pprint(portfolio)
    initialize_portfolio_information(portfolio)
    while True:
        print("\n")
        prices = wrapper.get_all_tickers()
        prices_dict = {prices["symbol"]: prices["price"] for prices in prices}
        rebalance_portfolio(portfolio)
        portfolio = get_portfolio(prices_dict)
        all_assets_percentage_in_btc(portfolio)
        get_market_rules(portfolio)
        time.sleep(6)


def get_market_rules(portfolio: Dict[str, Asset]):
    rules = wrapper.get_exchange_rules()
    if rules is not None:
        for current_asset, current_content in portfolio.items():
            if current_asset == wanted_stablecoin:
                current_symbol: str = "BTC" + wanted_stablecoin
            else:
                current_symbol = current_asset + current_content.market_trade
            for current_rule in rules:
                if current_rule["symbol"] == current_symbol:
                    for current_filter in current_rule["filters"]:
                        if current_filter["filterType"] == "PRICE_FILTER":
                            current_content.price_rules = current_filter
                        elif current_filter["filterType"] == "LOT_SIZE":
                            current_content.quantity_rule_for_market = current_filter
                        elif current_filter["filterType"] == "MIN_NOTIONAL":
                            current_content.min_notional_rule = current_filter
            portfolio[current_asset] = current_content


def initialize_portfolio_information(portfolio: Dict):
    wrapper.get_exchange_information()
    for current_asset_percentage in wanted_asset_in_bot:
        if wanted_asset_in_bot[current_asset_percentage] == 0:
            wanted_asset_in_bot[current_asset_percentage] = portfolio[
                current_asset_percentage
            ].total_btc_percentage


def get_portfolio(prices: Dict) -> Dict[str, Asset]:
    json_object = wrapper.get_account()
    portfolio = {}
    balances_dict = {
        balance["asset"]: balance["free"]
        for balance in json_object["balances"]
        if Decimal(balance["free"]) != 0
    }
    for current_asset, current_balance in balances_dict.items():
        asset_instance = Asset(current_asset)
        asset_instance.balance = Decimal(current_balance)
        if current_asset == "BTC":
            asset_instance.price = Decimal(prices["BTC" + wanted_stablecoin])
            asset_instance.total = Decimal(prices["BTC" + wanted_stablecoin]) * Decimal(
                current_balance
            )
            asset_instance.market_trade = wanted_stablecoin
        elif current_asset == wanted_stablecoin:
            asset_instance.price = 1 / Decimal(prices["BTC" + wanted_stablecoin])
            asset_instance.total = current_balance
        elif current_asset in wanted_asset_in_bot:
            asset_instance.price = Decimal(prices[current_asset + "BTC"])
            asset_instance.total = Decimal(prices[current_asset + "BTC"]) * Decimal(
                current_balance
            )
        else:
            continue
        portfolio[current_asset] = asset_instance
    return portfolio


def all_assets_percentage_in_btc(portfolio: Dict):
    global total_portfolio_value
    total_portfolio_value = Decimal(0)
    for current_asset, current_content in portfolio.items():
        if current_asset == wanted_stablecoin:
            continue
        elif current_asset == "BTC":
            total_portfolio_value += current_content.balance
        else:
            total_portfolio_value += current_content.total
    for current_asset, current_content in portfolio.items():
        if current_asset == wanted_stablecoin:
            current_content.total_btc_percentage = 0
        elif current_asset == "BTC":
            current_content.total_btc_percentage = (
                current_content.balance * 100
            ) / total_portfolio_value
        else:
            current_content.total_btc_percentage = (
                current_content.total * 100
            ) / total_portfolio_value
        portfolio[current_asset] = current_content
    total_portfolio_value = (
        total_portfolio_value * portfolio["BTC"].price
        + portfolio[wanted_stablecoin].balance
    )
    print("total portfolio value = " + str(total_portfolio_value))


def rebalance_portfolio(portfolio: Dict[str, Asset]):  # noqa:C901
    for current_asset in portfolio:
        if current_asset == "BTC":
            continue
        if current_asset == wanted_stablecoin:
            print(
                "total portfolio value in rebalance fonction = "
                + str(total_portfolio_value)
            )
            current_usd_amount = portfolio[current_asset].balance
            current_usd_percentage = current_usd_amount * 100 / total_portfolio_value
            logging.info(
                f"{current_asset} : amount {current_usd_amount:.8f} percentage {current_usd_percentage:.2f} "
                f"relative percentage {abs(current_usd_percentage - wanted_usd_in_bot):.2f}"
            )
            if current_usd_percentage < (
                wanted_usd_in_bot - threshold
            ) or current_usd_percentage > (wanted_usd_in_bot + threshold):
                quantity_of_wanted_usd = (
                    current_usd_amount / current_usd_percentage * wanted_usd_in_bot
                )
                logging.info("Entering USD / BTC trade to rebalance fiat")
                print("quantity_of_wanted_usd is : " + str(quantity_of_wanted_usd))
                print("quantity_of_current_usd is : " + str(current_usd_amount))
                if quantity_of_wanted_usd > current_usd_amount:
                    product = "BTC" + wanted_stablecoin
                    order_type = SIDE_SELL
                    usd_qty = round(quantity_of_wanted_usd - current_usd_amount, 8)

                else:
                    product = "BTC" + wanted_stablecoin
                    order_type = SIDE_BUY
                    usd_qty = round(current_usd_amount - quantity_of_wanted_usd, 8)
                chkd_usd_qty = check_and_apply_market_rules_for_asset(
                    usd_qty, portfolio[current_asset]
                )
                if chkd_usd_qty is not None:
                    logging.info(
                        f"OK : {order_type} : BTC for {str(chkd_usd_qty)}{current_asset}"
                    )
                    qty_of_btc = chkd_usd_qty * portfolio[current_asset].price
                    usd_stepsize = Decimal(
                        portfolio[current_asset].quantity_rule_for_market["stepSize"]
                    )
                    final_qty = round(qty_of_btc - (qty_of_btc % usd_stepsize), 8)
                    print("Wanted quantity is : " + str(final_qty))
                    create_market_order(
                        order_type,
                        final_qty,
                        product,
                    )
                else:
                    logging.error("Error during USD trade")
            continue
        current_asset_balance = portfolio[current_asset].balance
        current_asset_price = portfolio[current_asset].price
        current_asset_percentage = portfolio[current_asset].total_btc_percentage

        logging.info(
            f"{current_asset} : amount {current_asset_balance:.8f} price"
            f" {current_asset_price:.8f} percentage {current_asset_percentage:.2f} "
            f"relative percentage {abs(current_asset_percentage - wanted_asset_in_bot[current_asset]):.2f}"
        )
        if current_asset_percentage > (
            wanted_asset_in_bot[current_asset] + threshold
        ) or current_asset_percentage < (
            wanted_asset_in_bot[current_asset] - threshold
        ):
            logging.info(
                f"Entering {current_asset} trade to rebalance crypto portfolio"
            )
            total_wanted_asset = (
                current_asset_balance
                / current_asset_percentage
                * wanted_asset_in_bot[current_asset]
            )
            if total_wanted_asset > current_asset_balance:
                order_type = "BUY"
                qty_of_asset_to_trade = total_wanted_asset - current_asset_balance
            else:
                order_type = "SELL"
                qty_of_asset_to_trade = current_asset_balance - total_wanted_asset
            chkd_qty_of_asset_to_trade = check_and_apply_market_rules_for_asset(
                qty_of_asset_to_trade, portfolio[current_asset]
            )
            if chkd_qty_of_asset_to_trade is not None:
                logging.info(
                    f"OK : {order_type} : {str(chkd_qty_of_asset_to_trade)}{current_asset}"
                )
                try:
                    product = current_asset + portfolio[current_asset].market_trade
                    create_market_order(order_type, chkd_qty_of_asset_to_trade, product)
                except BinanceException as e:
                    logging.error(e.msg)
            else:
                logging.error("Can't send order, quantity is not authorized")


def check_and_apply_market_rules_for_asset(
    qty_of_asset_to_trade: Decimal, current_asset: Asset
) -> Optional[Decimal]:
    current_rule_min_qty = current_asset.quantity_rule_for_market["minQty"]
    current_rule_max_qty = current_asset.quantity_rule_for_market["maxQty"]
    current_rule_stepSize = current_asset.quantity_rule_for_market["stepSize"]
    if not (
        qty_of_asset_to_trade >= Decimal(current_rule_min_qty)
        and qty_of_asset_to_trade <= Decimal(current_rule_max_qty)
    ):
        logging.error("LOT_SIZE isn't respected")
        return None
    qty_of_asset_to_trade = round(
        qty_of_asset_to_trade
        - (qty_of_asset_to_trade - Decimal(current_rule_min_qty))
        % Decimal(current_rule_stepSize),
        8,
    )
    if current_asset.asset == wanted_stablecoin:
        if qty_of_asset_to_trade < Decimal(
            current_asset.min_notional_rule["minNotional"]
        ):
            logging.error(f"MIN_NOTIONAL for {wanted_stablecoin} isn't respected")
            return None
    elif current_asset.min_notional_rule["applyToMarket"]:
        if qty_of_asset_to_trade * current_asset.price < Decimal(
            current_asset.min_notional_rule["minNotional"]
        ):
            logging.error("MIN_NOTIONAL isn't respected")
            return None
    return qty_of_asset_to_trade


def create_market_order(type, amount, product):
    iterator = 0
    while iterator < 3:
        order = wrapper.create_order(product, type, amount)
        if order is None:
            logging.error(f"try number {iterator} failed")
        else:
            reporting.log_trade_order(order)
            return
        iterator = iterator + 1


def create_order():
    order = wrapper.create_order("XLMBTC", "BUY", 100, "0.00000500")
    print(order)


if __name__ == "__main__":
    main()
