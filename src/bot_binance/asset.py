from decimal import Decimal
from typing import Dict


class Asset:
    def __init__(self, asset: str):
        self.asset = asset
        self.balance = Decimal(0)
        self.price = Decimal(0)
        self.total = Decimal(0)  # for btc it's in dollar, all currencies in BTC
        self.total_btc_percentage = Decimal(0)
        self.market_trade = "BTC"
        self.price_rules: Dict[str, Decimal] = {}
        self.quantity_rule_for_market: Dict[str, Decimal] = {}
        self.min_notional_rule: Dict[str, Decimal] = {}

    def __str__(self) -> str:
        return f"{self.__dict__}"

    def __repr__(self) -> str:
        return str(self)
