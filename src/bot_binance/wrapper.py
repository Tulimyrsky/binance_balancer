import hashlib
import hmac
import logging
import os
import time
from typing import Dict, List, Optional, Tuple
from urllib.parse import urlencode, urljoin

import requests


def key_wrapper() -> Tuple[str, str]:
    tmp_api_key = os.getenv("B_KEY")
    if tmp_api_key is None:
        raise Exception("API_KEY environment varible must exist")
    tmp_secret_key = os.getenv("B_S_KEY")
    if tmp_secret_key is None:
        raise Exception("API_SECRET environment varible must exist")
    return tmp_api_key, tmp_secret_key


API_KEY, API_SECRET = key_wrapper()
BASE_URL = "https://api.binance.com"


headers = {"X-MBX-APIKEY": API_KEY}


def main():
    get_server_time()
    get_price("BTCUSDT")
    get_all_tickers()


class BinanceException(Exception):
    def __init__(self, status_code, data):

        self.status_code = status_code
        if data:
            self.code = data["code"]
            self.msg = data["msg"]
        else:
            self.code = None
            self.msg = None
        message = f"{status_code} [{self.code}] {self.msg}"

        super().__init__(message)


def get_exchange_rules() -> Optional[List[Dict]]:
    PATH = "/api/v1/exchangeInfo"
    params = None

    url = urljoin(BASE_URL, PATH)
    try:
        r = requests.get(url, params=params)
    except Exception as e:
        logging.exception(e)
        return None
    if r.status_code == 200:
        json = r.json()
        return json["symbols"]
    else:
        raise BinanceException(status_code=r.status_code, data=r.json())


def get_exchange_information():
    PATH = "/api/v1/exchangeInfo"
    params = {}

    url = urljoin(BASE_URL, PATH)
    try:
        r = requests.get(url, params=params)
    except Exception as e:
        logging.error(e.msg)
        return
    if r.status_code == 200:
        return r.json()
    else:
        raise BinanceException(status_code=r.status_code, data=r.json())


def get_server_time():
    PATH = "/api/v1/time"
    params = None

    timestamp = int(time.time() * 1000)

    url = urljoin(BASE_URL, PATH)
    try:
        r = requests.get(url, params=params)
    except Exception as e:
        logging.error(e.msg)
        return
    if r.status_code == 200:
        data = r.json()
        print(f"diff={timestamp - data['serverTime']}ms")
    else:
        raise BinanceException(status_code=r.status_code, data=r.json())


def get_price(symbol):
    PATH = "/api/v3/ticker/price"
    params = {"symbol": symbol}

    url = urljoin(BASE_URL, PATH)
    try:
        r = requests.get(url, headers=headers, params=params)
    except Exception as e:
        logging.exception(e)
        return None
    if r.status_code == 200:
        return r.json()
    else:
        raise BinanceException(status_code=r.status_code, data=r.json())


def get_all_tickers():
    PATH = "/api/v3/ticker/price"
    params = {}

    url = urljoin(BASE_URL, PATH)
    try:
        r = requests.get(url, headers=headers, params=params)
    except Exception as e:
        logging.exception(e)
        return None
    if r.status_code == 200:
        return r.json()
    else:
        raise BinanceException(status_code=r.status_code, data=r.json())


def create_order(symbol: str, order_side: str, amount, price=0):
    PATH = "api/v3/order"
    timestamp = int(time.time() * 1000)
    params = {
        "symbol": symbol,
        "side": order_side,
        "quantity": amount,
        "timestamp": timestamp,
    }
    if price == 0:
        params["type"] = "MARKET"
    else:
        params.update(
            {
                "type": "LIMIT",
                "timeInForce": "GTC",
                "price": price,
            }
        )

    query_string = urlencode(params)
    params["signature"] = hmac.new(
        API_SECRET.encode("utf-8"),
        query_string.encode("utf-8"),
        hashlib.sha256,
    ).hexdigest()

    url = urljoin(BASE_URL, PATH)
    try:
        r = requests.post(url, headers=headers, params=params)
    except Exception as e:
        logging.exception(e)
        return None
    if r.status_code == 200:
        data = r.json()
        return data
    else:
        raise BinanceException(status_code=r.status_code, data=r.json())


def get_account():
    PATH = "api/v3/account"
    timestamp = int(time.time() * 1000)
    params = {
        "timestamp": timestamp,
    }
    query_string = urlencode(params)
    params["signature"] = hmac.new(
        API_SECRET.encode("utf-8"),
        query_string.encode("utf-8"),
        hashlib.sha256,
    ).hexdigest()

    url = urljoin(BASE_URL, PATH)
    try:
        r = requests.get(url, headers=headers, params=params)
    except Exception as e:
        logging.exception(e)
        return None
    if r.status_code == 200:
        data = r.json()
        return data
    else:
        raise BinanceException(status_code=r.status_code, data=r.json())


if __name__ == "__main__":
    main()
